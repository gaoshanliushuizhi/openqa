package org.aksw.openqa.component.answerformulation.retriever.impl;

import java.net.URI;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Set;

import org.aksw.openqa.Properties;
import org.aksw.openqa.component.param.ResultMap;
import org.aksw.openqa.component.retriever.IRetrieverEntryInstanciator;
import org.aksw.openqa.component.retriever.PropertyEntry;
import org.aksw.openqa.component.retriever.ResultEntry;

import com.hp.hpl.jena.datatypes.BaseDatatype.TypedValue;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class EntryInstanciator implements IRetrieverEntryInstanciator<ResultMap, ResultEntry> {

	@Override
	public ResultMap newInstance(ResultEntry resultEntry) throws Exception {
		ResultMap result = new ResultMap();
		Set<Entry<String, PropertyEntry>> propertiyMap = resultEntry.listProperties();
		for(Entry<String, PropertyEntry> property : propertiyMap) {
			PropertyEntry propertyEntry = property.getValue();
			Class<?> type = propertyEntry.getType();
			Object value = propertyEntry.getValue();
			if(type.isAssignableFrom(URI.class)) {
				result.setParam(Properties.URI, value.toString());
			} else if(type.isAssignableFrom(Number.class)) {
				result.setParam(Properties.Literal.NUMBER, value);
			} else if (type.isAssignableFrom(Date.class)) {
				result.setParam(Properties.Literal.DATE, value);
			} else if(type.isAssignableFrom(Boolean.class)) {
				result.setParam(Properties.Literal.BOOLEAN, value);
			} else if(type.isAssignableFrom(TypedValue.class)) {
				TypedValue typedValue = (TypedValue) value;
				result.setParam(Properties.Literal.TEXT, typedValue.lexicalValue);				
			} else if(type.isAssignableFrom(String.class)) {
				result.setParam(Properties.Literal.TEXT, value);
			} else {
				throw new Exception("The instanciator could not handle the value " + value + " of type " + type);
			}
		}
		return result;
	}

	@Override
	public boolean stop() {
		return false;
	}
}
