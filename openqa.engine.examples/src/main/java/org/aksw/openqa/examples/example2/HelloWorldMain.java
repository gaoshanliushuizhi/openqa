package org.aksw.openqa.examples.example2;

import java.util.ArrayList;
import java.util.List;

import org.aksw.openqa.AnswerFormulation;
import org.aksw.openqa.Properties;
import org.aksw.openqa.SequentialStage;
import org.aksw.openqa.Stage;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.main.QueryResult;
import org.aksw.openqa.manager.plugin.PluginManager;

public class HelloWorldMain {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		String question = "";
		String answer = "";
		if(args.length > 0) {
			question  = args[0];
		}
		
		System.out.println("You:\t" + question);
		   	    	
    	HelloWorldQueryParser interpreter = new HelloWorldQueryParser();
    	HelloWorldRetriever retriever = new HelloWorldRetriever();
    	
    	PluginManager pluginManager = new PluginManager();
    	pluginManager.register(interpreter);
    	pluginManager.register(retriever);
    	pluginManager.setActive(true, interpreter.getId());
    	pluginManager.setActive(true, retriever.getId());
    	
    	AnswerFormulation queryProcessor = new AnswerFormulation();
    	
    	// pipeline instantiation    	
    	List<Stage> stages = new ArrayList<Stage>();
    	Stage stageInterpreter = new SequentialStage();
    	List<String> componentsIDs = new ArrayList<String>();
    	componentsIDs.add(interpreter.getId());
    	stageInterpreter.setComponentIDs(componentsIDs);
    	stages.add(stageInterpreter);
    	
    	Stage stageRetriever = new SequentialStage();
    	List<String> retrieverComponentIDs = new ArrayList<String>();
    	retrieverComponentIDs.add(retriever.getId());
    	stageRetriever.setComponentIDs(retrieverComponentIDs);
    	stages.add(stageRetriever);
    	
    	QueryResult result;
		result = queryProcessor.process(question, pluginManager, stages);
		List<? extends IResultMap> output = result.getOutput();
		if(output.size() > 0) {
			answer = output.get(0).getParam(Properties.Literal.TEXT, String.class);
		}
		
		System.out.println("openQA:\t" + answer);
		
	}

}
