package org.aksw.openqa.component;

import org.aksw.openqa.component.answerformulation.IQueryParser;
import org.aksw.openqa.component.answerformulation.IRetriever;
import org.aksw.openqa.component.answerformulation.ISynthesizer;
import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.service.IService;


public interface ISpecializedPluginVisitor {
	public void visit(IService service);	
	public void visit(IQueryParser interpreter);	
	public void visit(IRetriever retriever);
	public void visit(ISynthesizer synthesizer);
	public void visit(IContext context);
}
