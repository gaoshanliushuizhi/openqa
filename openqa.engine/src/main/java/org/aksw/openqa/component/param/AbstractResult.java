package org.aksw.openqa.component.param;


public abstract class AbstractResult extends AbstractMap implements IResultMap {
	@Override
	public void accept(IMapVisitor visitor) {
		visitor.visit(this);
	}
	
	public void printStackTrace() {
		PrintStackTraceResultVisitor ps = new PrintStackTraceResultVisitor();
		accept(ps);
	}
	
	public String toJSON() {
		JSONResultVisitor jsVisitor = new JSONResultVisitor();
		accept(jsVisitor);
		return jsVisitor.getJSON();
	}
}
