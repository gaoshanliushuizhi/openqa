package org.aksw.openqa.component;

import java.util.List;
import java.util.Map;

import org.aksw.openqa.component.context.IContext;
import org.aksw.openqa.component.param.IParamMap;
import org.aksw.openqa.component.param.IResultMap;
import org.aksw.openqa.component.param.IResultMapList;
import org.aksw.openqa.component.param.ResultMapListMetaInfo;
import org.aksw.openqa.component.param.ResultMetaInfo;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.log4j.Logger;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractPluginProcess extends AbstractPlugin implements IPluginProcess {
	
	private static Logger logger = Logger.getLogger(AbstractPluginProcess.class);
	
	public AbstractPluginProcess(Map<String, Object> params) {
		super(params);
	}
	
	public IResultMapList<? extends IResultMap> process(List<? extends IParamMap> params, 
			ServiceProvider serviceProvider, 
			IContext context) throws Exception {
		ResultMapListMetaInfo<IResultMap> totalResults = new ResultMapListMetaInfo<IResultMap>();
		totalResults.setInputParams(params);
		totalResults.setSource(this);
		for(IParamMap param : params) {
			try {
				List<? extends IResultMap> partialResults = process(param, serviceProvider, context);
				if(partialResults != null) {
					for(IResultMap result : partialResults) {
						ResultMetaInfo r = new ResultMetaInfo(result.getParameters(), param, this);
						totalResults.add(r);
					}
				}
			} catch (Exception e) {
				logger.error("Error processing parameters.", e);
			} catch (Error e) {
				logger.error("Error processing parameters.", e);
			}
		}
		return totalResults;
	}
	
	public boolean canProcess(List<? extends IParamMap> params) {
		// verify if can process all the params,		
		for(IParamMap param : params) {
			// if there is any param in the list that can not be processed, return false
			if(!canProcess(param)) {
				return false;
			}
		}
		return true;
	}
}
