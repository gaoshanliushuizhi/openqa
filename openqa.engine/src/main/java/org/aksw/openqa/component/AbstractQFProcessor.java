package org.aksw.openqa.component;

import java.util.Map;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractQFProcessor extends AbstractPluginProcess {
	public AbstractQFProcessor(Map<String, Object> params) {
		super(params);
	}
}