package org.aksw.openqa.component.retriever;


/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 * @param <L>
 */
public interface IResultEntryTriplestoreRetriever extends ITriplestoreRetriever<ResultEntry> {	
}
