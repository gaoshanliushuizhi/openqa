package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactory;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public abstract class AbstractSynthesizerFactorySpi extends AbstractPluginFactory<ISynthesizer> implements ISynthesizerFactory {
}
