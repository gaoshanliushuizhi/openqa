package org.aksw.openqa.component;

public interface IPluginVisitor {
	public void visit(IPlugin plugin);
}
