package org.aksw.openqa.component.param;

import java.util.List;

public interface IResultMapList<E extends IResultMap> extends List<E>, IMetaInfo {
}