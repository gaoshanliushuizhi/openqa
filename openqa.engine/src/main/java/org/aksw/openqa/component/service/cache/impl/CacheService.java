package org.aksw.openqa.component.service.cache.impl;

import java.util.HashMap;
import java.util.Map;

import org.aksw.openqa.component.service.AbstractService;
import org.aksw.openqa.component.service.cache.ICacheService;
import org.aksw.openqa.main.OpenQA;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public class CacheService extends AbstractService implements ICacheService {
	
	Map<String, Cache<String, Object>> caches = new HashMap<String, Cache<String, Object>>();
	public static final String MAX_CAPACITY = "MAX_CAPACITY";
	
	int capacity = 100;
	
	// DEFAULT PARAMS
	{
		setParam(MAX_CAPACITY, capacity);
	}
	
	public CacheService(Map<String, Object> params) {
		super(params);
	}
	
	@Override
	public void setParam(String param, Object o) {
		if(param.equals(MAX_CAPACITY)) {
			int capacity = Integer.parseInt(o.toString());
			for(Cache<String, Object>  cache : caches.values()) {
				cache.setCapacity(capacity);
			}
		}
		super.setParam(param, o);
	}

	public Cache<String, Object> get(String cacheContext) throws Exception {
		Cache<String, Object>  cache = caches.get(cacheContext);
		if(cache == null) {
			cache = new Cache<String, Object>(capacity);
			caches.put(cacheContext, cache);
		}
		return cache;
	}
	
	public Object get(String cacheContext, String objectID) throws Exception {
		Cache<String, Object> cache = get(cacheContext);
		return cache.get(objectID);
	}
	
	public <T extends Object> T get(String cacheContext, String objectID, Class<T> clazz) throws Exception {
		Cache<String, Object> cache = get(cacheContext);
		return clazz.cast(cache.get(objectID));
	}

	@Override
	public void put(String cacheContext, String objectID, Object value) throws Exception {
		Cache<String, Object> cache = get(cacheContext);
		cache.put(objectID, value);
	}

	@Override
	public String getVersion() {
		return OpenQA.ENGINE_VERSION;
	}
	
	@Override
	public String getAPI() {
		return OpenQA.API_VERSION;
	}
}
