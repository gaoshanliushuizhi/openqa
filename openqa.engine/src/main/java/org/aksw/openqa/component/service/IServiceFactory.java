package org.aksw.openqa.component.service;

import org.aksw.openqa.component.IPluginFactorySpi;

/**
 * 
 * @author {@linkplain http://emarx.org}
 *
 */
public interface IServiceFactory extends IPluginFactorySpi<IService> {
}
