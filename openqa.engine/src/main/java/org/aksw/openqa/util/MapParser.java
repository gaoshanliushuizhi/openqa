package org.aksw.openqa.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/** Co-opt args4j CmdLineParser to accept parameter maps instead.  */
public class MapParser
{
	private CmdLineParser parser;

	public MapParser(CmdLineParser parser) {this.parser=parser;}

	public void parse(Map<String,Object> args)
	{	
		// Java 8
		//		args.entrySet().stream().map(e->"-"+e.getKey()+"="+e.getValue()).collect(Collectors.toList()));		
		List<String> cmdArgs = new LinkedList<>();
		for(Entry<String,Object> e: args.entrySet())
		{
			cmdArgs.add("-"+e.getKey()+"="+e.getValue());
		}
		try{parser.parseArgument(cmdArgs);}
		catch(CmdLineException e) {throw new RuntimeException(e);}
	}
}