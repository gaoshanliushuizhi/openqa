package org.aksw.openqa.qald;

import java.io.Serializable;


public class QuestionResult implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2346446801347109026L;

	String questionID;

	double fmeasure;
	double precision;
	double recall;
	
	public String getID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}
	
	public double getFmeasure() {
		return fmeasure;
	}
	
	public void setFmeasure(double fmeasure) {
		this.fmeasure = fmeasure;
	}
	
	public double getPrecision() {
		return precision;
	}
	
	public void setPrecision(double precision) {
		this.precision = precision;
	}
	
	public double getRecall() {
		return recall;
	}
	
	public void setRecall(double recall) {
		this.recall = recall;
	}
	
	@Override
	public String toString() {
		return  "ID " + getID() +
				" Recall:" + getRecall() +
				" Precision:" + getPrecision() +
				" F-measure:" + getFmeasure();
	}
}
