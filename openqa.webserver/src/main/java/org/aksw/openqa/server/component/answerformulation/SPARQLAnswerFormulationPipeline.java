package org.aksw.openqa.server.component.answerformulation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.aksw.openqa.Stage;

public class SPARQLAnswerFormulationPipeline extends  ArrayList<Stage> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1049401154018108931L;

	public SPARQLAnswerFormulationPipeline() {
		Stage stage0 = new Stage();
		Set<String> componentIDs = new HashSet<String>();
		stage0.setId("QueryParser");
		componentIDs.add("QueryParserProvider");
		stage0.setComponentIDs(componentIDs);		
		add(stage0);
	}
}
